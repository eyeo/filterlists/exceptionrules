# Exceptionrules

This project contains filter rules for the Acceptable Ads program.

***IMPORTANT:*** After cloning the project, follow the installation steps in order for the pre-commit hook to work.


## Installation

### Windows

1. Turn on Developer Mode in `Settings -> System -> For developers`.
2. Run the following commands in order to set up the linux subsystem environment and create a symbolic link for the pre-commit hook:

```sh
export MSYS=winsymlinks:nativestrict
wsl --update
wsl --install -d Ubuntu-24.04
wsl -s Ubuntu-24.04
wsl sudo apt update
wsl sudo apt install jq
ln -s -f "$(pwd)/pre-commit-src/pre-commit" "$(pwd)/.git/hooks/pre-commit"
```

### macOS

The script requires a newer version of bash, which is not the default on macOS. Install it using Homebrew and create a symbolic link for the pre-commit hook::

```sh
brew install bash
ln -s -f "$(pwd)/pre-commit-src/pre-commit" "$(pwd)/.git/hooks/pre-commit"
```

### Linux

Everything should work out of the box. Create a symbolic link for the pre-commit hook:

```sh
ln -s -f "$(pwd)/pre-commit-src/pre-commit" "$(pwd)/.git/hooks/pre-commit"
```

## Adding New Domain Variables Files

When adding or renaming files, remember to update the template files and inform the team responsible for delivering the filterlist.
